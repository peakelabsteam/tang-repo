using System.Web.UI;

namespace TangierConfiguration.BaseAppCode
{
	public class AlertTemplate : ITemplate
	{

		private Page _page;

		public AlertTemplate(Page page)
		{
			this._page = page;
		}

		public void InstantiateIn(Control owner)
		{
			Control ctrl = _page.LoadControl("~/Include/AlertTemplate.ascx");
			owner.Controls.Add(ctrl);
		}
	}
}